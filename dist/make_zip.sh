#!/bin/bash

# Run from inside the dist directory

NAME=qdsync-0.1

rm ../$NAME.zip
cd ../..
mv qdsync $NAME

zip -r $NAME.zip $NAME -x "*.pyc" -x "*~" -x "*/CVS/*" -x "*.cvsignore"

mv $NAME qdsync
mv $NAME.zip qdsync/

