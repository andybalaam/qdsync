class LsResult:
	def __init__( self ):
		self.dirs  = []
		self.files = []
		self.links = []

class LsFileResult:
	def __init__( self, filedir, filename, size, date ):
		self.filedir  = filedir
		self.filename = filename
		self.size = size
		self.date = date

class LsDirResult:
	def __init__( self, dirname ):
		self.dirname = dirname

class LsLinkResult:
	def __init__( self, filedir, filename, linkpath, date ):
		self.filedir  = filedir
		self.filename = filename
		self.linkpath = linkpath
		self.date = date

