# This class specifies a series of actions that will be performed in order
# to synchronise 2 directories "1" and "2".  The actions are broken up into
# several categories e.g. newcopy1to2, deletefrom1

import os
import qdsync_ls_result, qdlog

overwrite1to2_string	   = " --overwrite-> "
newcopy1to2_string		 = " -----new----> "
todeletefrom1_string	   = " X--delete	 "
todeletefrom1silent_string = " x--clean	  "
overwrite2to1_string	   = " <-overwrite-- "
newcopy2to1_string		 = " <----new----- "
todeletefrom2_string	   = "	 delete--X "
todeletefrom2silent_string = "	  clean--x "

class ActionSpec:
	def __init__( self, dir1_fs, dir2_fs, term_width = None,
			logsilentdeletions = False ):
		self.dir1_fs = dir1_fs
		self.dir2_fs = dir2_fs
		if term_width != None:
			self.term_width = int( term_width )
		else:
			cols = os.getenv( 'COLUMNS' )   # TODO: This doesn't seem to work...
			if cols != None:
				self.term_width = int( cols )
			else:
				self.term_width = 80
		self.logsilentdeletions = logsilentdeletions
		self.overwrite1to2	   = []
		self.newcopy1to2		 = []
		self.todeletefrom1	   = []
		self.todeletefrom1dirs   = []
		self.todeletefrom1silent = []
		self.overwrite2to1	   = []
		self.newcopy2to1		 = []
		self.todeletefrom2	   = []
		self.todeletefrom2dirs   = []
		self.todeletefrom2silent = []
	
	def sortEntries( self ):
		self.overwrite1to2.sort()
                self.newcopy1to2.sort()
                self.todeletefrom1.sort()
                self.todeletefrom1dirs.sort()
                self.todeletefrom1silent.sort()
                self.overwrite2to1.sort()
                self.newcopy2to1.sort()
                self.todeletefrom2.sort()
                self.todeletefrom2dirs.sort()
                self.todeletefrom2silent.sort()


	def getFullReport( self ):
		colmid_width = len( overwrite1to2_string )
		col_width = int( ( self.term_width - colmid_width ) / 2 )
		title1_reduced = self._reduceToWidth( self.dir1_fs.describe(),
			col_width )
		title2_reduced = self._reduceToWidth( self.dir2_fs.describe(),
			col_width )
		
		ans = ( "%%%ds" + " " * colmid_width + "%%s\n" ) % col_width % (
			title1_reduced, title2_reduced )
		ans += "-" * self.term_width + "\n"
		
		for fl in self.newcopy1to2:
			ans += self._formatRow( fl, newcopy1to2_string, "*", col_width )
		for fl in self.newcopy2to1:
			ans += self._formatRow( "*", newcopy2to1_string, fl, col_width )
		
		for fl in self.overwrite1to2:
			ans += self._formatRow( fl, overwrite1to2_string, fl, col_width )
		for fl in self.overwrite2to1:
			ans += self._formatRow( fl, overwrite2to1_string, fl, col_width )
		
		for fl in self.todeletefrom1:
			ans += self._formatRow( fl, todeletefrom1_string, "", col_width )
		for dr in self.todeletefrom1dirs:
			ans += self._formatRow( dr, todeletefrom1_string, "", col_width )
			
		if( self.logsilentdeletions ):
			for dr in self.todeletefrom1silent:
				ans += self._formatRow( dr, todeletefrom1silent_string, "",
					col_width )
		
		for fl in self.todeletefrom2:
			ans += self._formatRow( "", todeletefrom2_string, fl, col_width )
		for dr in self.todeletefrom2dirs:
			ans += self._formatRow( "", todeletefrom2_string, dr, col_width )
		
		if( self.logsilentdeletions ):
			for dr in self.todeletefrom2silent:
				ans += self._formatRow( "", todeletefrom2silent_string, dr,
					col_width )
		
		return ans
	
	def run( self ):
		extra_width = len( overwrite1to2_string ) + len( qdlog._program_name )
		col_width = int( ( self.term_width - extra_width ) / 2 ) 
		
		for fl in self.newcopy1to2:
			qdlog.Logger().logInfo( self._formatRow( fl, 
				newcopy1to2_string, "*", col_width, False ) )
			self.dir2_fs.mkdirs( fl.filedir )
			reader = self.dir1_fs.openForRead( fl )
			writer = self.dir2_fs.openForWrite( fl )
			while( not reader.eof() ):
				writer.write( reader.read() )
			reader.close()
			writer.close()
		
		for fl in self.newcopy2to1:
			qdlog.Logger().logInfo( self._formatRow( "*", 
				newcopy2to1_string, fl, col_width, False ) )
			self.dir1_fs.mkdirs( fl.filedir )
			reader = self.dir2_fs.openForRead( fl )
			writer = self.dir1_fs.openForWrite( fl )
			while( not reader.eof() ):
				writer.write( reader.read() )
			reader.close()
			writer.close()
			
		for fl in self.overwrite1to2:
			qdlog.Logger().logInfo( self._formatRow( fl, 
				overwrite1to2_string, fl, col_width, False ) )
			self.dir2_fs.mkdirs( fl.filedir )
			reader = self.dir1_fs.openForRead( fl )
			writer = self.dir2_fs.openForWrite( fl )
			while( not reader.eof() ):
				writer.write( reader.read() )
			reader.close()
			writer.close()
		
		for fl in self.overwrite2to1:
			qdlog.Logger().logInfo( self._formatRow( fl, 
				overwrite2to1_string, fl, col_width, False ) )
			self.dir1_fs.mkdirs( fl.filedir )
			reader = self.dir2_fs.openForRead( fl )
			writer = self.dir1_fs.openForWrite( fl )
			while( not reader.eof() ):
				writer.write( reader.read() )
			reader.close()
			writer.close()
		
		for fl in self.todeletefrom1:
			qdlog.Logger().logInfo( self._formatRow( fl, 
				todeletefrom1_string, "", col_width, False ) )
			self.dir1_fs.rm( fl )
		
		for dr in self.todeletefrom1dirs:
			qdlog.Logger().logInfo( self._formatRow( dr, 
				todeletefrom1_string, "", col_width, False ) )
			self.dir1_fs.rmdir( dr )
			
		for fl in self.todeletefrom1silent:
			if self.logsilentdeletions:
				qdlog.Logger().logInfo( self._formatRow( fl, 
					todeletefrom1silent_string, "", col_width, False ) )
			self.dir1_fs.rm( fl )
		
		for fl in self.todeletefrom2:
			qdlog.Logger().logInfo( self._formatRow( fl, 
				todeletefrom1_string, "", col_width, False ) )
			self.dir2_fs.rm( fl )
		
		for dr in self.todeletefrom2dirs:
			qdlog.Logger().logInfo( self._formatRow( dr, 
				todeletefrom1_string, "", col_width, False ) )
			self.dir2_fs.rmdir( dr )
		
		for fl in self.todeletefrom2silent:
			if self.logsilentdeletions:
				qdlog.Logger().logInfo( self._formatRow( "", 
					todeletefrom2silent_string, fl, col_width, False ) )
			self.dir2_fs.rm( fl )
	
	def anythingToDo( self ):
		return ( self.overwrite1to2
			  or self.newcopy1to2
			  or self.todeletefrom1
			  or self.todeletefrom1dirs
			  or self.todeletefrom1silent
			  or self.overwrite2to1
			  or self.newcopy2to1
			  or self.todeletefrom2 
			  or self.todeletefrom2dirs
			  or self.todeletefrom2silent )
		
	def _reduceToWidth( self, string, width ):
		ans = string
		if len( ans ) > width:
			less_than_half_width = int( width / 2 ) - 1
			ans = ans[:less_than_half_width] + ".." \
				+ ans[-less_than_half_width:]
			
		return ans
	
	def _prettyPath( self, dirname, path, width ):
		if dirname != "":
			ans = dirname + "/" + path
		else:
			ans = path
		return self._reduceToWidth( ans, width )
	
	def _formatRow( self, fldr1, string, fldr2, col_width, linebreak = True ):
		str1 = self._fldrtostr( fldr1, col_width )
		str2 = self._fldrtostr( fldr2, col_width )
		ans = "%%%ds%%s%%s" % col_width % ( str1, string, str2 )
		if linebreak:
			ans += "\n"
		return ans
	
	def _fldrtostr( self, fldr, col_width ):
		if fldr.__class__ == qdsync_ls_result.LsFileResult:
			ans = self._prettyPath( fldr.filedir, fldr.filename, col_width )
		elif fldr.__class__ == qdsync_ls_result.LsDirResult:
			ans = self._prettyPath( fldr.dirname, "", col_width )
		else:
			ans = fldr
		return ans
		
