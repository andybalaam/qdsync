# Superclass of all the filesystem classes - an abstract interface class

class Fs:
	
	# Change to the supplied directory, either relative or absolute path
	# Understands ".." to mean the next path up, but does not necessarily
	# understand more complex dir specs.
	# (I.e. it is acceptable to make this not understand paths containing
	# slashes - all we need is to be able to change into immediate subdirs,
	# and go up one when ".." is supplied.)
	def cd( self, path ):
		raise Exception( "Not implemented." )
	
	# Return the present working directory relative to the path originally
	# supplied when the Fs object was created.  Always uses forward slashes
	# as a path separator.
	def pwd( self ):
		raise Exception( "Not implemented." )
	
	# List the files, dirs and symlinks in the current directory
	# Returns a LsResult object like this:
	# res.dirs = [ dir1, dir2, dir3, ... ]
	# res.files = [ file1, file2, file3, ... ]
	# res.links = [ symlink1, symlink2, ... ]
	# Where each file is a LsFileResult itself with properties:
	# filename, size, date (size is in bytes, date is a datetime tuple thingy)
	# dirs are LsDirResult object with one property - dirname
	# and each symlink is a LsLinkResult with filename, linkpath and date
	def ls( self ):
		raise Exception( "Not implemented." )
	
	# Given a base path and another, join them and return the result.
	# Usually just inserts a slash (if needed) and concatenates.
	# As everywhere else, always returns a path using forward slashes as
	# separators.
	def pathJoin( self, path1, path2 ):
		raise Exception( "Not implemented." )
	
	# Create the supplied directory and its parents
	def mkdirs( self, path ):
		raise Exception( "Not implemented." )
	
	# Delete the supplied file
	def rm( self, lsFileResult ):
		raise Exception( "Not implemented." )
	
	# Delete the supplied dir, which must be empty
	def rmdir( self, lsDirResult ):
		raise Exception( "Not implemented." )
	
	# Describe the filesystem being represented: giving machine path etc.
	def describe( self ):
		raise Exception( "Not implemented." )
	
	# Open the supplied file for reading and return a FileReader object.
	def openForRead( self, lsFileResult ):
		raise Exception( "Not implemented." )
	
	# Open the supplied file for writing and return a FileWriter object.
	def openForWrite( self, lsFileResult ):
		raise Exception( "Not implemented." )
	
# Returned by the openForRead() method above
class FileReader:
	
	# Open this file for reading
	def __init__( self, fullpath ):
		raise Exception( "Not implemented." )
	
	# Read some bytes from the file and return them
	def read( self ):
		raise Exception( "Not implemented." )
	
	# Return True if the end of the file has been reached
	def eof( self ):
		raise Exception( "Not implemented." )
	
	# Close the file
	def close( self ):
		raise Exception( "Not implemented." )

# Returned by the openForWrite() method above
class FileWriter:
	
	# Open this file for writing
	def __init__( self, fullpath ):
		raise Exception( "Not implemented." )
	
	# Write some bytes to the file
	def write( self, bytes ):
		raise Exception( "Not implemented." )
	
	# Close the file
	def close( self ):
		raise Exception( "Not implemented." )




