import qdsync_fs, re

# Use this filesystem type if this regular expression matches the url
spec_re_1 = re.compile( "ssh://([^:@ ]*)@([^:@ ]*):([^:@ ]*)" )
spec_re_2 = re.compile( "([^:@ ]*)@([^:@ ]*):([^:@ ]*)" )

# Factory method - if this spec matches, create an object, otherwise ret. None
def createFs( fs_spec, qdsyncMain ):
	retFS = None
	
	match = spec_re_1.match( fs_spec )
	if match == None:
		match = spec_re_2.match( fs_spec )
	
	if match != None:
		grs = match.groups()
		retFS = SshFs( grs[0], grs[1], grs[2] )
	
	return retFS

# The class that implements this filesystem
class SshFs( qdsync_fs.Fs ):
	
	def __init__( self, username, host, path ):
		print "SSH: username=%s host=%s path=%s" % ( username, host, path )

