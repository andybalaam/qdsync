# Quick and Log - a logging system used by qdsync and qdmirror

# Copyright (C) 2006-2007 Andy Balaam

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

import sys

_program_name = "qdlog: "
_warning = "Warning: "
_error = "Error: "
class Logger:
	
	class __impl:
		
		def __init__( self ):
			self._loglevel = "warning"
			self._debuglogtypes = []
		
		def setLogLevel( self, level ):
			self._loglevel = level
		
		def addDebugLogType( self, logtype ):
			self._debuglogtypes.append( logtype )
		
		def die( self, message, retval = 1 ):
			self.logError( message )
			sys.exit( retval )
			
		def logError( self, message ):
			sys.stderr.write( _program_name + _error + message + "\n" )
		
		def logWarning( self, message ):
			if( self._loglevel == "warning"
			 or self._loglevel == "info" ):
				sys.stderr.write( _program_name + _warning + message + "\n" )
		
		def logInfo( self, message ):
			if self._loglevel == "info":
				 sys.stderr.write( _program_name + message + "\n" )
		
		def logDebug( self, logtype, message ):
			if logtype in self._debuglogtypes:
				sys.stderr.write( _program_name + logtype + ": " + message
					+ "\n" )
		
		def addParserOptions( self, parser ):
			parser.add_option("", "--debug-logging", dest="debug_logging", 
				help="A pipe(|)-separated list of debug strings to output.",
				default="" )
		
		def processParserOptions( self, options ):
			debug_strings = options.debug_logging.split( "|" )
			for s in debug_strings:
				self.addDebugLogType( s )
	
	__instance = None
	
	def __init__(self):
		if Logger.__instance is None:
			Logger.__instance = Logger.__impl()

		self.__dict__['_Logger__instance'] = Logger.__instance

	def __getattr__( self, attr ):
		return getattr( self.__instance, attr )

	def __setattr__( self, attr, value ):
		return setattr( self.__instance, attr, value )

