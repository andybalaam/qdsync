import qdsync_fs, qdsync_ls_result, qdlog, re, os, time, stat

# Use this filesystem type if this regular expression matches the url
spec_re = re.compile( "^([^:@]*)$" )

# Factory method - if this spec matches, create an object, otherwise ret. None
def createFs( fs_spec, qdsyncMain ):
	retFS = None
	match = spec_re.match( fs_spec )
	if match != None:
		grs = match.groups()
		retFS = LocalFs( grs[0] )
	
	return retFS

# fullpath2details
# { fullpath2 : [ [dir1, dir2, dir3 ], [file1, file2, file3] ],
#				 [dir1, dir2, dir3 ], [file1, file2, file3] ],
#				  ...
#			   ,
#   fullpath2 : [ [dir1, dir2, dir3 ], [file1, file2, file3] ],
#			   [ [dir1, dir2, dir3 ], [file1, file2, file3] ],
#				  ...
#			   , ...
# }

# The class that implements this filesystem
class LocalFs( qdsync_fs.Fs ):
	
	def __init__( self, path ):
		self.toppath = self._preparePath( path )
		self.curpath = self.toppath
		self.fullpath2details = {}
	
	def cd( self, path ):
		self.curpath = self._preparePath( os.path.join( self.curpath, path ) )
	
	def pwd( self ):
		return self._pathRelToTopDir( self.curpath )
	
	def ls( self ):
		ansLsResult = qdsync_ls_result.LsResult()
		names = os.listdir( self.curpath )
		
		for name in names:
			fullpath = self._preparePath( os.path.join( self.curpath, name ) )
			
			if os.path.islink( fullpath ):
				ansLsResult.links.append( qdsync_ls_result.LsLinkResult(
					self._pathRelToTopDir( self.curpath ), name,
					self._pathRelToTopDir( 
						os.path.realpath( fullpath ) ),
					time.gmtime( os.path.getmtime( fullpath ) ) ) )
				qdlog.Logger().logDebug( "Local_files",
					"Local link: %s/%s -> %s " % ( self.curpath, name, 
						self._pathRelToTopDir( os.path.realpath( fullpath ) ) ))
					
			elif os.path.isdir( fullpath ):
				ansLsResult.dirs.append( qdsync_ls_result.LsDirResult( name ) )
				
			elif os.path.isfile( fullpath ):
				stat_struct = os.stat( fullpath )
				ansLsResult.files.append( qdsync_ls_result.LsFileResult(
					self._pathRelToTopDir( self.curpath ), name,
					os.path.getsize( fullpath ),
					time.gmtime( os.path.getmtime( fullpath ) ) ) )
					
			else:
				print "File '%s' is not a link, directory or a path."
		return ansLsResult
	
	def pathJoin( self, path1, path2 ):
		return self._preparePath( os.path.join( path1, path2 ) )
	
	def mkdirs( self, path ):
		fullpath = self.pathJoin( self.toppath, path )
		if not os.path.exists( fullpath ):
			os.makedirs( fullpath )
	
	def rm( self, lsFileResult ):
		os.remove( self.pathJoin( self.toppath, 
			self.pathJoin( lsFileResult.filedir, lsFileResult.filename ) ) )
	
	def rmdir( self, lsDirResult ):
		os.rmdir( self.pathJoin( self.toppath, lsDirResult.dirname ) )
	
	def describe( self ):
		return "Local dir %s" % ( self.toppath )
	
	def openForRead( self, lsFileResult ):
		return LocalFileReader(
			self.pathJoin( self.toppath,
				self.pathJoin( lsFileResult.filedir, lsFileResult.filename ) ) )
	
	def openForWrite( self, lsFileResult ):
		return LocalFileWriter(
			self.pathJoin( self.toppath,
				self.pathJoin( lsFileResult.filedir, lsFileResult.filename ) ) )
	
	def _preparePath( self, path ):
		return os.path.normpath( os.path.normcase( path ) )
		
	def _pathRelToTopDir( self, path ):
		toppath = self.toppath
		toppathplusslash = toppath + "/"
		
		len_toppathplusslash = len( toppathplusslash )
		
		if path[:len_toppathplusslash] == toppathplusslash:
			ans = path[len_toppathplusslash:]
		else:
			len_toppath = len( toppath )
			if path[:len_toppath] == toppath:
				ans = path[len_toppath:]
			else:
				qdlog.Logger().logDebug( "Local_warning", "path does not begin "
					"with toppath, when it was expected to." )
				# End this sentence with a preposition.  It's quick and dirty.
				ans = path
		return ans

class LocalFileReader( qdsync_fs.FileReader ):
	
	def __init__( self, fullpath ):
		self._fl = file( fullpath, 'r' )
		self._cached_read = ""
	
	def read( self ):
		if self._cached_read == "":
			self._cached_read = self._fl.read()
		ans = self._cached_read
		self._cached_read = ""
		return ans
	
	def eof( self ):
		self._cached_read += self._fl.read()
		return ( self._cached_read == "" )
	
	def close( self ):
		self._fl.close()

class LocalFileWriter( qdsync_fs.FileWriter ):
	
	def __init__( self, fullpath ):
		self._fl = file( fullpath, 'w' )
	
	def write( self, bytes ):
		self._fl.write( bytes )
	
	def close( self ):
		self._fl.close()
