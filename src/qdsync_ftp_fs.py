import qdsync_fs, qdsync_ls_result, qdlog, re, time

from ftplib import FTP
import ftplib

# TODO: Non-standard ports

# Use this filesystem type if this regular expression matches the url
spec_re_1 = re.compile( "ftp://([^: ]*):([^: ]*)@([^:@ ]*):([^:@ ]*)" )
spec_re_2 = re.compile( "ftp://([^: ]*)@([^:@ ]*):([^:@ ]*)" )

ls_re = re.compile( "([ld-])[r-][w-][x-][r-][w-][x-][r-][w-][x-]\\s*" # perms
	+ "\\d*\\s*"			# unknown
	+ "[0-9a-zA-Z]*\\s*"	# group
	+ "[0-9a-zA-Z]*\\s*"	# user
	+ "([0-9a-zA-Z]*)\\s*"  # size
	+ "([A-Z][a-z][a-z]\\s*\\d\\d?\\s*[0-9:]*)\\s*"	# date/time
	+ "(.*)\\s*"		  # filename + possible symlink
	)

dateyear_re = re.compile( "([A-Z][a-z][a-z])\\s*" # Month
	+ "(\\d\\d?)\\s*"   # Day
	+ "(\\d\\d\\d\\d)\\s*"  # Year
	)

datetime_re = re.compile( "([A-Z][a-z][a-z])\\s*" # Month
	+ "(\\d\\d?)\\s*"   # Day
	+ "(\\d\\d:\\d\\d)\\s*"  # Time
	)

symlink_re = re.compile( "\\s*(.*)\\s*->\\s*(.*)\\s*" );

pathjoin_re = re.compile( "(.*/)[^/]*/\\.\\./(.*)" )

lastdir_re = re.compile( ".*/(.*?)" )

# Factory method - if this spec matches, create an object, otherwise ret. None
def createFs( fs_spec, qdsyncMain ):
	retFS = None
	
	match = spec_re_1.match( fs_spec )
	if match != None:
		grs = match.groups()
		retFS = FtpFs( qdsyncMain, grs[0], grs[1], grs[2], grs[3] )
	
	else:
		match = spec_re_2.match( fs_spec )
		if match != None:
			grs = match.groups()
			retFS = FtpFs( qdsyncMain, grs[0], "", grs[1], grs[2] )
	
	return retFS

# The class that implements this filesystem
class FtpFs( qdsync_fs.Fs ):
	
	def __init__( self, qdsyncMain, username, password, host, path ):
		
		if password == None:
			dqsyncMain.exit( "For the moment, you have to supply the FTP password on the command line, sorry." );
		
		self.qdsyncMain = qdsyncMain
		self.username = username
		self.password = password
		self.host = host
		if path[:1] == "/":
			self.path = path
		else:
			self.path = "/" + path
		self.ftp = None
		self.cachedpwd = self.path
		self.dirsmade = set()
	
	def cd( self, newpath ):
		self._ensureConnected()
		self.ftp.cwd( newpath )
		qdlog.Logger().logDebug( "FTP_dirs", "cd: " + newpath )
		self.cachedpwd = self.ftp.pwd()
		qdlog.Logger().logDebug( "FTP_dirs", "pwd after cd: " + self.cachedpwd )
	
	def pwd( self ):
		if self.cachedpwd == None:
			self.cachedpwd = self.ftp.pwd()
			qdlog.Logger().logDebug( "FTP_dirs", "Setting cached pwd: "
				+ self.cachedpwd )
		qdlog.Logger().logDebug( "FTP_dirs", "Returning cached pwd: "
			+ self.cachedpwd )
		
		return self._pathRelToTopDir( self.cachedpwd )
	
	def ls( self ):
		self._ensureConnected()
		self.tmpLsResult = qdsync_ls_result.LsResult()
		# TODO: catch timeouts
		self.ftp.dir( self._lsLine )
		return self.tmpLsResult
	
	def pathJoin( self, path1, path2 ):
		qdlog.Logger().logDebug( "FTP_pathJoin", "Supplied paths: "
			"'%s', '%s'" % ( path1, path2 ) )
		# Stick the 2 paths together (or use 2 if begins with /)
		if path2[:1] == "/" or len( path1 ) == 0:
			ans = path2
		else:
			ans = path1 + "/" + path2
		# Strip out any ..s
		match = pathjoin_re.match( ans )
		while match != None:
			grs = match.groups()
			ans = grs[0] + grs[1]
			match = pathjoin_re.match( ans )
		qdlog.Logger().logDebug( "FTP_pathJoin", "Answer: '%s'" % ans )
		return ans
	
	# Create the supplied directory and its parents
	def mkdirs( self, path ):

		if path in self.dirsmade:
			return

		self.dirsmade.add( path )

		if path[:1] == "/":
			self.cd( "/" )
		
		splitpath = path.split( "/" )
		for pathpart in splitpath:
			if len( pathpart ) > 0:
				qdlog.Logger().logDebug( "FTP_mkdirs", "Creating directory %s "
					% pathpart )
				try:
					self.ftp.mkd( pathpart )
				except ftplib.error_perm:
					# Probably dir already exists
					qdlog.Logger().logDebug( "FTP_mkdirs",
						"Already exists or no permission" )
				self.cd( pathpart )
		# Now cd back to where we were
		for pathpart in splitpath:
			if len( pathpart ) > 0:
				self.cd( ".." )
	
	# Delete the supplied file
	def rm( self, lsFileResult ):
		self.cd( "/" )
		fullpath = self._combinePath( self.path, lsFileResult.filedir )
		self.cd( fullpath )
		qdlog.Logger().logDebug( "FTP_rm", "Removing %s /%s " %
			( fullpath, lsFileResult.filename ) )
		self.ftp.delete( lsFileResult.filename )
	
	# Delete the supplied dir, which must be empty
	def rmdir( self, lsDirResult ):
		self.cd( "/" )
		fullpath = self._combinePath( self.path, lsDirResult.dirname )
		self.cd( fullpath )
		self.cd( ".." )
		
		qdlog.Logger().logDebug( "FTP_rmdir", "CDing to: %s" %
			fullpath )
		
		match = lastdir_re.match( lsDirResult.dirname )
		if match:
			todel = match.group( 0 )
		else:
			todel = lsDirResult.dirname
			
		qdlog.Logger().logDebug( "FTP_rmdir", "Deleting: %s" % todel )
		self.ftp.rmd( todel )
	
	def describe( self ):
		return "FTP %s@%s:%s" % ( self.username, self.host, self.path )
	
	def openForRead( self, lsFileResult ):
		return FTPFileReader(
			self.pathJoin( self.path,
				self.pathJoin( lsFileResult.filedir, lsFileResult.filename ) ),
			self.ftp )
	
	def openForWrite( self, lsFileResult ):
		return FTPFileWriter(
			self.pathJoin( self.path,
				self.pathJoin( lsFileResult.filedir, lsFileResult.filename ) ),
			self.ftp )
	
	
	def _lsLine( self, line ):
		match = ls_re.match( line )
		if match != None:
			(ld, size, datetime, filename_symlink) = match.groups()
			
			if ld == "-":   # This is a normal file
				# TODO: interpret file sizes in MB etc?
				flres = qdsync_ls_result.LsFileResult(
					self._pathRelToTopDir( self.cachedpwd ), filename_symlink,
					int( size ),
					self._getRealDateTime( datetime ) )
				self.tmpLsResult.files.append( flres ) 
				qdlog.Logger().logDebug( "FTP_files",
					"file: %s / %s" % (flres.filedir, flres.filename ) )
			
			elif ld == "d": # This is a directory
				if filename_symlink not in ( "..", "." ):
					drres = qdsync_ls_result.LsDirResult( filename_symlink )
					self.tmpLsResult.dirs.append( drres )
					qdlog.Logger().logDebug( "FTP_files",
						"dir: " + drres.dirname )
			
			elif ld == "l": # This is a symlink
				match = symlink_re.match( filename_symlink )
				if match != None:
					lnres = qdsync_ls_result.LsLinkResult(
						self._pathRelToTopDir( self.cachedpwd ),
						match.group(1), match.group(2),
						self._getRealDateTime( datetime ) )
					self.tmpLsResult.links.append( lnres )
					qdlog.Logger().logDebug( "FTP_files",
						"link: " + lnres.filename )
				
			else:
				print "Unrecognised ls character '%s'" % ld
				
		else:
			print "Unrecognised ls line '%s'" % line
	
	def _ensureConnected( self ):
		if self.ftp == None:
			self.ftp = FTP( self.host, self.username, self.password )
			self.ftp.cwd( self.path )
	
	def _combinePath( self, path1, path2 ):
		if( path1 == "" ):
			return path2
		elif( path2 == "" ):
			return path1
		else:
			return path1 + "/" + path2
	
	def _getRealDateTime( self, datetime ):
		if dateyear_re.match( datetime ):
			realdatetime = time.strptime( datetime, "%b %d %Y" )
		elif datetime_re.match( datetime ):
			realdatetime = time.strptime(
				datetime + " %s" % time.gmtime()[0], "%b %d %H:%M %Y" )
			# If adding this year makes this date in the future, remove one
			# year to make it last year
			if realdatetime > time.gmtime():
				realdatetime = ( realdatetime[0] - 1, realdatetime[1],
					realdatetime[2], realdatetime[3], realdatetime[4],
					realdatetime[5], realdatetime[6], realdatetime[7],
					realdatetime[8] )
		else:
			print "Unrecognised date string '%s'" % datetime
			realdatetime = None
		
		return realdatetime
	
	def _pathRelToTopDir( self, path ):
		toppath = self.path
		toppathplusslash = toppath + "/"
		
		len_toppathplusslash = len( toppathplusslash )
		
		if path[:len_toppathplusslash] == toppathplusslash:
			ans = path[len_toppathplusslash:]
		else:
			len_toppath = len( toppath )
			if path[:len_toppath] == toppath:
				ans = path[len_toppath:]
			else:
				qdlog.Logger().logDebug( "FTP_warning", "path does not begin "
					"with toppath, when it was expected to.  path='%s' "
					"toppath='%s" % ( path, toppath ) )
				# End this sentence with a preposition.  It's quick and dirty.
				ans = path
		return ans

class FTPFileReader( qdsync_fs.FileReader ):
	
	def __init__( self, filename, ftp ):
		qdlog.Logger().logDebug( "FTPFileReader", "read: " + filename )
		self._cached_read = ""
		self._eof = False
		ftp.retrbinary( "RETR %s" % filename, self._readcallback )
		
	def read( self ):
		ans = self._cached_read
		self._cached_read = ""
		self._eof = True
		return ans
	
	def eof( self ):
		return self._eof
	
	def close( self ):
		pass
		
	def _readcallback( self, bytes ):
		self._cached_read += bytes
		
class FTPFileWriter( qdsync_fs.FileWriter ):
	
	def __init__( self, filename, ftp ):
		qdlog.Logger().logDebug( "FTPFileWriter", "write:" + filename )
		self._filename = filename
		self._pretendfile = _FTPPretendFile()
		self._ftp = ftp
	
	def write( self, bytes ):
		self._pretendfile.addbytes( bytes )
		pass
	
	def close( self ):
		self._ftp.storbinary( "STOR %s" % self._filename, self._pretendfile )

class _FTPPretendFile:
	
	def __init__( self ):
		self._cached_bytes = ""
	
	def addbytes( self, bytes ):
		self._cached_bytes += bytes

	def read( self, num_bytes ):
		ans = self._cached_bytes[:num_bytes]
		self._cached_bytes = self._cached_bytes[num_bytes:]
		return ans

	def eof( self ):
		return ( self._cached_bytes == "" )

