To run qdsync in its current alpha state, simply give its full path.  For example:

$ cd ~/my_web_site
$ ~/downloads/qdsync-0.1/src/qdsync --interactive ./ ftp://ftp.mywebsite.com:/public_html

For more info and examples, type:

man doc/qdsync.1

